import os
import sys
import time
import pygame

pygame.mixer.init()

tic_tac_1min = pygame.mixer.Sound("sounds/tic_tac_1min.ogg")
bell = pygame.mixer.Sound('sounds/bell.ogg')

def working(minutes):
	while minutes > 0:
		minutes -= 1
		tic_tac_1min.play()
		os.system('cls')
		print('   *_____*   ')
		print('  *_*****_*  ')
		print(' *_(O)_(O)_* ')
		print('**____V____**')
		print('**_________**')
		print('**_________**')
		print(' *_________* ')
		print('  ***___***  \n')
		print('Working...', minutes+1, 'min')
		time.sleep(60)

def pause(minutes):
	bell.play()
	while minutes > 0:
		minutes -= 1
		s = 60;
		while s > 0:
			time.sleep(1)
			s -= 1
			os.system('cls')
			print('Pause...')
			print(minutes, 'min', s, 'sec')

def start(m1 = 25, m2 = 5):
	if m1 < 0:
		m1 = 25
	if m2 < 0:
		m2 = 5
	while True:
		working(m1)
		pause(m2)

if __name__=='__main__':
	print(len(sys.argv))
	if len(sys.argv) > 2:
		work = int(sys.argv[1])
		p = int(sys.argv[2])
		start(work, p)
	else:
		start()